import React, {useState} from 'react'

export default function TextForm(props) {
    const convetedToUppercase = () => {
    let newtext = text.toUpperCase();
        changeText(newtext);
        props.showAlertState('Text is capitlized', 'success');
    }
    const handleOnChange = (evt) => {
        changeText(evt.target.value);
    }
    const clearTextHandler = (evt) => {
        changeText('');
        props.showAlertState('Text is Cleared', 'success');
    }
    const getAsArrayHandler = () => {
        console.log(text.split(' '));
    }
    const getnewStringHandler = () => {
        let newText = text.slice(0, 4);

    }
   
    const emailProtect = () => {
        if(email.length > 0) {
     let splitedEmail = email.split('@');
     let part1 = splitedEmail[0];
    const avg = Math.round(part1.length /2);
        part1 = part1.substring(0, (part1.length - avg));
        const part2 = splitedEmail[1];
        let newEmail = `${part1}...@${part2}`;
        changeEmail(newEmail);
    } else {
        changeEmail('Email is should not empty');
    }
}
    const firstLatterCap = () => {
        let newArray = [];
        const extractLetter = name.split('');
        extractLetter.forEach(char => {
            let firstCharacter = char.at(0).toUpperCase();
             const newCharacters = firstCharacter + char.substring(1).toLowerCase();
             newArray.push(newCharacters);
             
        });
        changename(newArray.join(' '));
        

    }
    const extractEmail = (evt) => {
        changeEmail(evt.target.value);
    }
    const extractname = (evt) => {
        changename(evt.target.value);
    }
const firstLetterCapitlize = (word) => {
     const lower = word.toLowerCase();
     
}
  const [email, changeEmail] = useState('');
  const [name, changename] = useState('');
  const [text, changeText] = useState('');
    return (
        <>
        <div className="form-group">
            <h1>{props.heading}</h1>
            <div className="mb-3">
                <input type="text" value={name} onChange={extractname}/>
                <input type="email" value={email} onChange={extractEmail} />
                <textarea  className="form-control" onChange={handleOnChange} id="mybox" rows="3" value={text} style={{backgroundColor: props.mode === 'light'? 'white': 'grey'}}></textarea>
        </div>
        <button onClick= {convetedToUppercase} className="btn btn-primary">Convert to Uppercase </button>
        <button onClick= {clearTextHandler} className="btn btn-primary mx-2">Clear Text </button>
        <button onClick= {getAsArrayHandler} className="btn btn-primary mx-2">Split </button>
        <button onClick= {getnewStringHandler} className="btn btn-primary mx-2">get new String </button>
        <button onClick= {emailProtect} className="btn btn-primary mx-2">get Email Protect </button>
        <button onClick= {firstLatterCap} className="btn btn-primary mx-2">get Name </button>
      </div>
      <div className="container">
          <h2>Summanry</h2>
          Word count {text.split(' ').length} character Counter {text.length} <br/>
          {text}
          Protected Email {email}
      </div>
      <div>{name}</div>
      </>
    )
}
