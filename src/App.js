import './App.css';
import Navbar from './components/Navbar';
import TextForm from './components/TextForm';
import About from './components/About';
import React, { useState } from 'react'
import Alert from './components/Alert';
import { BrowserRouter as Router,  Switch,  Route,  Link } from "react-router-dom";

function App() {
  const [mode, modestate] = useState('light');
  const [alert, alertState] = useState(null);
  document.title = 'Light mode';
  const toggleMode = () => {
    if(mode === 'light') {
      showAlertState('this is dark mode', 'success');
      modestate('dark');
      document.title = 'Dark mode';
    } else {
      modestate('light');
      showAlertState('this is light mode', 'success');
      document.title = 'Light mode';
    }
  }
  const showAlertState = (message, type) => {
    console.log(message)
    alertState({
      msg: message,
      type: type
    });
    setTimeout(() => {
      alertState(null);
    }, 1500);
  }
  return (
  <>
  <Router>
      <Navbar title='utilText' mode={mode} toggleMode={toggleMode} aboutTxt= 'About us' />
      <Alert alert={alert}/>
      <div className='container my-3'>
      <Switch>
      <Route exact path="/">
          <TextForm heading="First text" mode={mode} showAlertState={showAlertState} />
          </Route>
          <Route exact path="/about">
            <About />
          </Route>
          
         
        </Switch>
      </div>
      </Router>
  </>

  )
}

export default App;
